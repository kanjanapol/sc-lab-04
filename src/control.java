

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class control {
	class Listenermgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent evt) {
			
			
			window.word = (String) window.comboBox.getSelectedItem();
			String str2 = window.textField.getText();
			int count = Integer.parseInt(str2);
			
			if (window.word == "1") {
				window.output.setText(test.nest1(count));
			}
			if (window.word == "2") {
				window.output.setText(test.nest2(count));
			}
			if (window.word == "3") {
				window.output.setText(test.nest3(count));
			}
			if (window.word == "4") {
				window.output.setText(test.nest4(count));
			}
			if (window.word == "5"){
				window.output.setText(test.nest5(count));
			}
			

		}
	}

	public static void main(String[] args) {
		new control();

	}

	public control() {
		window = new gui();
		test = new nestloop();
		window.frame.setVisible(true);
		window.setSize(600, 400);
		window.setLocation(400, 270);
		list = new Listenermgr();
		window.setListener(list);

	}

	gui window;
	ActionListener list;
	nestloop test;
}