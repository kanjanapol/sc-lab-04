

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class gui extends JFrame {
	JFrame frame;
	JTextField textField;
	JButton run;
	String word;
	JComboBox comboBox;
	JTextArea output ;
	
	
	public gui() {
		creategui();
	}

	public void creategui() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		output = new JTextArea();
		Font font1 = new Font("SansSerif", Font.BOLD, 20);
		output.setBounds(10, 75, 414, 192);
		output.setFont(font1);
		frame.getContentPane().add(output);

		run = new JButton("Run\r\n");
		run.setBounds(319, 40, 89, 23);
		frame.getContentPane().add(run);

		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "1", "2","3", "4", "5" }));
		comboBox.setBounds(24, 40, 106, 20);
		frame.getContentPane().add(comboBox);

		textField = new JTextField();
		textField.setBounds(152, 40, 120, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblPattern = new JLabel("\t\t\tChose Pattern");
		lblPattern.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPattern.setForeground(Color.DARK_GRAY);
		lblPattern.setBounds(24, 10, 98, 23);
		frame.getContentPane().add(lblPattern);
		
		JLabel lblNewLabel = new JLabel("Rows & Colums");
		lblNewLabel.setForeground(Color.DARK_GRAY);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(157, 11, 120, 20);
		frame.getContentPane().add(lblNewLabel);
	}

	public void setListener(ActionListener list) {

		run.addActionListener(list);
	}
}
