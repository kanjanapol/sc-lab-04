

import javax.swing.JFrame;

public class nestloop extends JFrame {

	public String nest1(int size) {
		String temp = "";
		for (int i = 1; i <= size-1; i++) {
			for (int j = 1; j <= size; j++) {
				temp = temp + "*";
			}
			temp = temp + "\n";
		}
		return temp;
	}

	public String nest2(int size) {
		String temp = "";
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size-1; j++) {
				temp = temp + "*";
			}
			temp = temp + "\n";
			
		}
		return temp;
	}

	public String nest3(int size) {
		String temp = "" ;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= i; j++)
				{temp = temp+"*";
				}
			temp = temp + "\n";
			}
		return temp;

	}

	public String nest4(int size) {
		String temp = "" ;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size ; j++) {
				if (j % 2 == 0) {
					temp = temp+"*";
				} else {
					temp = temp+"-";
				}
			}
			temp = temp + "\n";
		}
		return temp ;
	}

	public String nest5(int size) {
		String temp = "" ;
		for (int i = 1; i <= size; i++) {
			for (int j = 1; j <= size ; j++) {
				if ((i + j) % 2 == 0) {
					temp = temp+"*";
				} else {
					temp = temp+" ";
				}
			}
			temp = temp + "\n";
		}
		return temp ;
	}

}
